package br.com.scss.repairbook.domain.user;

import jakarta.persistence.*;

import java.util.Date;
@Entity
@Table(name="users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private Date registrationDate;
    private Date lastUpdateDate;
    private String fullName;
    private int countryCode;
    private String areaCode;
    private String phoneNumber;
    private int country;
    private String State;
    private String zipCode;
    private String address;
    private String addressNumber;

    public int getId() {
        return id;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public String getFullName() {
        return fullName;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public String getAreaCode() {
        return areaCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getCountry() {
        return country;
    }

    public String getState() {
        return State;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getAddress() {
        return address;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setCountryCode(int countryCode) {
        this.countryCode = countryCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public void setState(String state) {
        State = state;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }
}
