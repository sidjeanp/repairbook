package br.com.scss.repairbook.domain.repair;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="repairsbook")
public class RepairBook {
    @Id
    private Long id;
    private String descriptionRepair;
    private Integer typeSchedule;
    private Integer schedule;

}
