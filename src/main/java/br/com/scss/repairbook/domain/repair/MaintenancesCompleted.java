package br.com.scss.repairbook.domain.repair;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import java.util.Date;

@Entity
@Table(name="MaintenancesCompleted")
public class MaintenancesCompleted {
    @Id
    private Long id;
    private Date dateCompleted;
    private Integer idRepairShop;
    private Double totalParts;
    private Double totalLabor;

}
