package br.com.scss.repairbook.domain.vehicle;

import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name="vehicles")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private boolean publicInformations;
    private Date registrationDate;
    private Date lastUpdateDate;
    private int brand;
    private int descriptionBrand;
    private int model;
    private int descriptionModel;
    private String chassis;
    private String numberPlate;
    private int mainColor;
    private int year;
    private int modelYear;

    public int getId() {
        return id;
    }

    public boolean isPublicInformations() {
        return publicInformations;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public int getBrand() {
        return brand;
    }

    public int getModel() {
        return model;
    }

    public int getDescriptionBrand() {
        return descriptionBrand;
    }

    public int getDescriptionModel() {
        return descriptionModel;
    }

    public String getChassis() {
        return chassis;
    }

    public String getNumberPlate() {
        return numberPlate;
    }

    public int getMainColor() {
        return mainColor;
    }

    public int getYear() {
        return year;
    }

    public int getModelYear() {
        return modelYear;
    }

    public void setPublicInformations(boolean publicInformations) {
        this.publicInformations = publicInformations;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public void setBrand(int brand) {
        this.brand = brand;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public void setNumberPlate(String numberPlate) {
        this.numberPlate = numberPlate;
    }

    public void setMainColor(int mainColor) {
        this.mainColor = mainColor;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setModelYear(int modelYear) {
        this.modelYear = modelYear;
    }
}
