package br.com.scss.repairbook.domain.repair;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="items_maintenance_completed")
public class ItemsMaintenanceCompleted {
    @Id
    private Long id;
    private Integer typeItem;
    private String DescriptionPart;
    private String partNumber;
    private Integer quantity;
    private Double coast;

}
