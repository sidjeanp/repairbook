package br.com.scss.repairbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RepairbookApplication {

	public static void main(String[] args) {
		SpringApplication.run(RepairbookApplication.class, args);
	}

}
