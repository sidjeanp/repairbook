package br.com.scss.repairbook.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/")
    public String index(Model model){
        model.addAttribute("msnBemVindo","Seja bem-vindo ao Repair Book");
        model.addAttribute("msnDescricao","Queremos ajudar à todos");
        return "public-index";
    }

}
